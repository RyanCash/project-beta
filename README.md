# CarCar

Team:

- Justin Burgonio - Service microservice
- Person 2 - Which microservice?

## Design

Welcome to CarCar! This application is designed for car dealerships to manage their inventory, services and eomployees.

We have two framworks that are being used for this project. The first is Django, which is a Python framework. The second is React, which is a JavaScript framework. This combination allows us to create a full stack application that is both powerful and easy to use.

In CarCar there are three microservies. The first is the sales microservice, which is responsible for managing different aspects of car sales such as adding new sales, maintaining sales records, and handling staff. The second is the service microservice, which is responsible for managing the technicians, scheduling of appointments, managing said appointments and the ability to view the service history. The inventory microservice which is the last microservice, uses a poller to check and update the inventory if there are any changes to the inventory. It also allows the user to view the inventory and add new or remove cars from the inventory.

## CarCar Diagram

![carcar](https://i.ibb.co/txHYCq2/carcar.png)

## Getting Started

Please ensure you have access to the Docker Desktop application and your terminal.

### In your terminal

1. Change you directory to a folder you would like to clone the project to.
2. Run `git clone https://gitlab.com/RyanCash/project-beta.git`
3. Change your directory to the project folder `cd project-beta`

### Docker Desktop

Before completing the steps below please ensure that your Docker Desktop application is running. In your terminal type the commands below while making sure to press enter after
each command.

1. `docker volume create beta-data`
2. `docker-compose build`
3. `docker-compose up`

## Service microservice

This microservice allows the user to manage the technicians, scheduling of appointments, managing said appointments and the ability to view the service history.

Technicians can be created, listed and deleted. Each technician has to have unique employee ID which can be a combination of letters and numbers.

Appointments can be created, listed and deleted. When an appointment is created, it is then added to the service history as "created". Users have the ability to update the status of the appointment to "finished" or "cancelled".

### Models

1. AutomobileVo
2. Technician
3. Appointment

The Appointment model incorporates the Technician model through a foreign key relationship, maintaining a connection between the two entities while preserving their individual definitions.

### RESTful API calls

1. Technicians
   | Action | Method | URL |
   | ------------------------- | ------ | -------------------------------------------- |
   | List Technicians | GET | http://localhost:8080/api/technicians/ |
   | Create Technicians | POST | http://localhost:8080/api/technicians/|
   | Delete Technicians| DELETE | http://localhost:8080/api/technicians/:id/|
      <details>
      <summary>GET: Will return a list of all technicians. Employee ID given to the technican can be a mix of numbers or characters. ID is already given. The following is the JSON data that's returned</summary>
      JSON request body:

   ```json
   {
     "technicians": [
       {
         "first_name": "Bill",
         "last_name": "Nye",
         "employee_id": "scienceguy1",
         "id": 1
       }
     ]
   }
   ```

   </details>

      <details>
      <summary>POST: Will create a new technician. The following is the JSON data that's posted.</summary>
      json request body:

   ```json
   {
     "first_name": "Bill",
     "last_name": "Gates",
     "employee_id": "Microsoft1"
   }
   ```

   </details>

      <details>
      <summary>DELETE: Will delete a technician. Must put the correct ID in the URL.</summary>
      json request body:

   ```json
   {
     "deleted": true
   }
   ```

2. Appointments
   | Action | Method | URL |
   | ------------------------- | ------ | -------------------------------------------- |
   | List Appointments | GET | http://localhost:8080/api/appointments/ |
   | Create Appointments | POST | http://localhost:8080/api/appointments/ |
   | Delete Appointments | DELETE | http://localhost:8080/api/appointments/:id |
   | Finish Appointments | PUT | http://localhost:8080/api/appointments/:id/finish/ |
   | Cancel Appointments | PUT | http://localhost:8080/api/appointments/:id/cancel/ |
      <details>
      <summary>GET: Will return a list of all appointments. The following is the JSON data that's returned.</summary>
      json request body:

   ```json
   {
     "appointments": [
       {
         "date_time": "2023-03-28T01:38:00+00:00",
         "reason": "Flooded Engine",
         "status": "finished",
         "vin": "12345678901234567",
         "customer": "Justin Burgonio",
         "id": 42,
         "technician": {
           "first_name": "Bill",
           "last_name": "Nye",
           "employee_id": "ScienceGuy1",
           "id": 1
         },
         "VIP": false
       }
     ]
   }
   ```

   </details>
   <details>
      <summary>POST: Will create an appointment. The following is the JSON data that's posted.</summary>
      json request body:

   ```json
   {
     "date_time": "2023-05-01T10:00:00Z",
     "reason": "Transmission",
     "status": "created",
     "vin": "123456789012345ab",
     "customer": "Will Smith",
     "technician": 1
   }
   ```

   </details>
   <details>
      <summary>DELETE: Will delete an appointment. Must put the correct ID in the URL.</summary>
      json request body:
      
   ```json
   {
   "deleted": true
   }
   ```
   </details>
   <details>
      <summary>PUT: Will update the status of an appointment to "finished" or "canceled. Must put the correct ID in the URL.The following is the JSON data that is returned.</summary>
      
   ```json
   {
   "date_time": "2023-03-28T01:38:00+00:00",
   "reason": "Transmission",
   "status": "canceled",
   "vin": "123456789012345ab",
   "customer": "Will smith",
   "id": 1,
   "technician": {
   	"first_name": "Bill",
   	"last_name": "Nye",
   	"employee_id": "ScienceGuy1",
   	"id": 1
   },
   "VIP": false
   }

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

## models

## Inventory microservice

The inventory microservice is responsible for managing the inventory of cars. It allows the user to view the inventory and add new cares to the inventory.

To ensure a smooth ride, you must first establish a manufacturer to which you can assign various models. After setting up a manufacturer, users can then create and associate models with the appropriate manufacturer. Once a model is in place, users can generate an automobile by specifying its production year, color, and VIN for the chosen model. With these details in place, the automobile is now ready to hit the sales floor!

## Models

1. Manufacturer
2. VechileModel
3. Automobile

The VehicleModel model establishes a connection with the Manufacturer model through a foreign key relationship. Similarly, the Automobile model is linked to the VehicleModel model using a foreign key, ensuring both models maintain their unique definitions while staying interconnected.

1.  Manufacturer
    | Action | Method | URL |
    | ------------------------- | ------ | -------------------------------------------- |
    | List of Manufacturers | GET | http://localhost:8100/api/manufacturers/ |
    | Create Manufacturer | POST | http://localhost:8100/api/manufacturers/ |
    | Delete Manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/ |
    | Update Manufacturer | PUT | http://localhost:8080/api/appointments/:id/finish/ |
      <details>
         <summary>GET: Will return a list of all manufacturers.The following is the JSON data that's returned.</summary>
         json request body:

    ```json
    {
      "manufacturers": [
        {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Toyota"
        }
      ]
    }
    ```

      </details>
      <details>
         <summary>POST: Will create a new manufacturer. The following is the JSON data that's needed for the request.</summary>
         json request body:

    ```json
    {
      "name": "Toyota"
    }
    ```

      </details>
     <details>
         <summary>DELETE: Will delete a manufacturer. Must put the correct manufacturer ID in the URL. The folowing code is the returned data</summary>

    ```json
    {
      "id": null,
      "name": "Toyota"
    }
    ```

      </details>
      <details>
         <summary>PUT: Will update the manufacturer. Must put the correct manufacturer ID in the URL.  The following is the JSON data that is needed.
         </summary>
         json request body:

    ```json
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Honda"
    }
    ```

      </details>

2.  Vehicle Model
    | Action | Method | URL |
    | ------------------------- | ------ | -------------------------------------------- |
    | List Models | GET | http://localhost:8100/api/models/ |
    | Create Models | POST | http://localhost:8100/api/models/ |
    | Delete Models | DELETE | http://localhost:8100/api/manufacturers/:id/ |
    | Update Models | PUT | http://localhost:8100/api/manufacturers/:id/|
    <details>
    <summary>GET: Will return a list of all vehicle models. The following is the JSON data that's returned.</summary>
    JSON response body:

    ```json
    {
      "models": [
        {
          "href": "/api/models/1/",
          "id": 1,
          "name": "GR86",
          "picture_url": "https://s3.us-east-2.amazonaws.com/dealer-inspire-vps-vehicle-images/stock-images/chrome/87b31a45a2639d4841213af4d75e421f.png",
          "manufacturer": {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "Toyota"
          }
        }
      ]
    }
    ```

       </details>
       <details>
        <summary>POST: Will create a vehicle model. The following is the JSON data that's needed for the request.</summary>
        JSON request body:

    ```json
    {
      "name": "Gr86",
      "picture_url": "https://s3.us-east-2.amazonaws.com/dealer-inspire-vps-vehicle-images/stock-images/chrome/87b31a45a2639d4841213af4d75e421f.png",
      "manufacturer_id": 1
    }
    ```

       </details>
       <details>
        <summary>DELETE: Will delete vehicle model.Must put the correct manufacturer ID in the URL. The following code is the returned data.</summary>

    ```json
    {
      "id": null,
      "name": "Gr86",
      "picture_url": "https://s3.us-east-2.amazonaws.com/dealer-inspire-vps-vehicle-images/stock-images/chrome/87b31a45a2639d4841213af4d75e421f.png",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Toyota"
      }
    }
    ```

    </details>
    <details>
        <summary>PUT: Will update vehicle model.Must put the correct manufacturer ID in the URL. The following is the JSON data that is needed.</summary>
        JSON request body:

    ```json
    {
      "id": null,
      "name": "Gr86",
      "picture_url": "https://s3.us-east-2.amazonaws.com/dealer-inspire-vps-vehicle-images/stock-images/chrome/87b31a45a2639d4841213af4d75e421f.png",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Toyota"
      }
    }
    ```

    </details>

3.  Automobiles
    | Action | Method | URL |
    | ------------------------- | ------ | -------------------------------------------- |
    | List Automobiles | GET | http://localhost:8100/api/automobiles/ |
    | Create Automobiles | POST | http://localhost:8100/api/automobiles// |
    | Delete Automobiles | DELETE | http://localhost:8100/api/automobiles/:id/ |
    | Update Automobiles | PUT | http://localhost:8100/api/automobiles/:id/|
    <details>
    <summary>GET: This will return a list of automobiles. The following is the JSON data that's returned.</summary>
    JSON request body:

    ```json
    "autos": [
    		{
    			"href": "/api/automobiles/1C3CC5FB2AN120174/",
    			"id": 1,
    			"color": "Gray",
    			"year": 2022,
    			"vin": "1C3CC5FB2AN120174",
    			"model": {
    				"href": "/api/models/1/",
    				"id": 1,
    				"name": "GR86",
    				"picture_url": "https://s3.us-east-2.amazonaws.com/dealer-inspire-vps-vehicle-images/stock-images/chrome/87b31a45a2639d4841213af4d75e421f.png",
    				"manufacturer": {
    					"href": "/api/manufacturers/1/",
    					"id": 1,
    					"name": "Toyota"
    				}
    			},
    			"sold": false
    		},
    ]
    ```

      </details>
      <details>

      <summary>POST: This will create a new automobile. The following is the JSON data that's needed for the request.</summary>
      JSON request body:

    ```json
    {
      "color": "Grey",
      "year": 2022,
      "vin": "1C3CC5FB2AN120174",
      "model_id": 1
    }
    ```

      </details>
      <details>
      <summary>DELETE: This will delete the automobile from the database. Must put the correct manufacturer ID in the URL.</summary>
      URL: `http://localhost:8100/api/automobiles/:id`

      </details>
      <details>
      <summary>PUT: This will update the automobile. Must put the correct manufacturer ID in the URL.</summary>
      JSON request body:

    ```json
    {
      "color": "Grey",
      "year": 2022,
      "sold": true
    }
    ```

      </details>
