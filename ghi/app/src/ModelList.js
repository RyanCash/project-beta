import React, { useEffect, useState } from 'react';

const VehicleModels = () => {
    const [Vehicle, setVehicle] = useState([]);
    const [manufacturers, setManufacturers] = useState([]);

    const fetchVehicles = async () => {
        const vehicleUrl = "http://localhost:8100/api/models/";
        const response = await fetch(vehicleUrl);
        if (response.ok) {
        const data = await response.json();
        setVehicle(data.models);
        }
    };


    const fetchManufacturers = async () => {
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturerUrl);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => {
        fetchVehicles();
        fetchManufacturers();
    }, []);

    return (
        <>
          <h1>Vehicle Models</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Picture</th>
                <th>Model</th>
                <th>Manufacturer</th>
              </tr>
            </thead>
            <tbody>
              {Vehicle.map(vehicle => {
                return (
                  <tr key={vehicle.href}>
                    <td>{vehicle.name}</td>
                    <td>{vehicle.manufacturer.name}</td>
                    <td className="w-25">
                      <img src={vehicle.picture_url} className="img-thumbnail img"></img>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      );
    }

export default VehicleModels;





