import React, {useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';


function NewAppointmentForm() {
    const [technicians, setTechnicians] = useState([])
    const [customer, setCustomer] = useState('');
    const [vin, setVin] = useState("");
    const [date, setDate] = useState("");
    const [time, setTime] = useState("");
    const [technician, setTechnician] = useState('');
    const [reason, setReason] = useState('');
    const [status, setStatus] = useState('');
    const navigate = useNavigate();

    const handleCustomer = async (event) => {
      const value = event.target.value;
      setCustomer(value);
  }

  const handleVin = async (event) => {
          const value = event.target.value;
          setVin(value);
      }

    const handleDate = async (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTime = async (event) => {
        const value = event.target.value;
        setTime(value);
    }

    const handleReason = async (event) => {
      const value = event.target.value;
      setReason(value);
  }

    const handleTechnician = async (event) => {
      const value = event.target.value;
      setTechnician(value);
  }

  const handleStatus = async (event) => {
    const value = event.target.value;
    setStatus(value);
  }

    const data = {
      customer: customer,
      vin: vin,
      date_time: date + " " + time,
      reason: reason,
      technician: technician,
      status: status,
    }

    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            "Content-type": "application/json",
        },
    };

    const fetchData = async () =>{
      const url = "http://localhost:8080/api/technicians/";
      const response = await fetch(url);
      if (response.ok){
        const data = await response.json();
        setTechnicians(data.technicians);
      }
    };
  
    useEffect(() => {
      fetchData();
    }, []);
  

    const handleSubmit = async (event) => {
        event.preventDefault();
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            console.log(newTechnician)
            setCustomer("");
            setVin("");
            setDate("");
            setTime("");
            setReason("");
            setTechnician("");
            setStatus("");
            navigate('/appointments');
        }
    };
  

    return(
        <div className="my-5 container">
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a Service Appointment</h1>
                <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                    <input
                    value={customer}
                    onChange={handleCustomer}
                    placeholder="Customer"
                    required
                    type="text"
                    name="Customer"
                    id="Customer"
                    className="form-control"
                    />
                    <label htmlFor="Customer">Customer</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                    value={vin}
                    onChange={handleVin}
                    placeholder="Vin Number"
                    required
                    type="text"
                    name="Vin Number"
                    id="Vin Number"
                    className="form-control"
                    />
                    <label htmlFor="vin">Vin Number</label>
                  </div>
                  <div className="mb-3">
                    <input
                    value={date}
                    onChange={handleDate}
                    placeholder="Date"
                    required
                    type="date"
                    name="Date"
                    id="Date"
                    className="form-label"
                    />
                    <label htmlFor="name">Date</label>
                  </div>
                  <div className="floating mb-3">
                  <input
                    value={time}
                    onChange={handleTime}
                    placeholder="Time"
                    required
                    type="time"
                    name="Time"
                    id="Time"
                    className="form-label"
                  />
                  <label htmlFor="time">Time</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={reason}
                    onChange={handleReason}
                    placeholder="Reason"
                    required
                    type="text"
                    name="Reason"
                    id="Reason"
                    className="form-control"
                  />
                  <label htmlFor="reason">reason</label>
                </div>
                <div className="mb-3">
                  <select
                  value={technician}
                  onChange={handleTechnician}
                  required
                  name = "technician"
                  id="technician"
                  className="form-select"
                >
                  <option value="">Choose a technician</option>
                  {technicians.map((technician) =>{
                    return (
                      <option key={technician.id} value={technician.id}>
                        {technician.first_name} {technician.last_name}
                      </option>
                    );
                  })}
                  </select>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
  );
}

export default NewAppointmentForm;