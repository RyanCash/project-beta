import React from "react";

function AppointmentsList(props) {
  const handleCancelAppointment = async (id) => {
    const appointmentCancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
    const response = await fetch(appointmentCancelUrl, { method: "PUT" });
    if (response.ok) {
      const updatedAppointments = props.appointments.filter(
        (appointment) => appointment.id !== id
      );
      props.onUpdateAppointments(updatedAppointments);
    }
  };
  const handleFinishAppointment = async (id) => {
    const appointmentFinishUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
    const response = await fetch(appointmentFinishUrl, { method: "PUT" });
    if (response.ok) {
      const updatedAppointments = props.appointments.filter(
        (appointment) => appointment.id !== id
      );
      props.onUpdateAppointments(updatedAppointments);
    }
  }
  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th style={{ textAlign: "center" }}>Vin</th>
            <th style={{ textAlign: "center" }}>Is VIP?</th>
            <th style={{ textAlign: "center" }}>Customer</th>
            <th style={{ textAlign: "center" }}>Date</th>
            <th style={{ textAlign: "center" }}>Time</th>
            <th style={{ textAlign: "center" }}>Reason</th>
            <th style={{ textAlign: "center" }}>Technician</th>
            <th style={{ textAlign: "center" }}>Actions</th>
          </tr>
        </thead>
        <tbody>
          {props.appointments?.map((appointment) => {
            return (
            <tr key={appointment.id}>
              <td style={{ textAlign: "center" }}>{ appointment.vin }</td>
              <td style={{ textAlign: "center" }}>{ appointment.is_vip ? "Yes" : "No" }</td>
              <td style={{ textAlign: "center" }}>{ appointment.customer }</td>
              <td style={{ textAlign: "center" }}>{ new Date(appointment.date_time).toLocaleTimeString() }</td>
              <td style={{ textAlign: "center" }}>{ new Date(appointment.date_time).toLocaleDateString() }</td>
              <td style={{ textAlign: "center" }}>{ appointment.reason }</td>
              <td style={{ textAlign: "center" }}>
                {appointment.technician.first_name} {appointment.technician.last_name}
                </td>
                <td className="text-center">
                  <button
                  className="btn btn-danger"
                  onClick={() => handleCancelAppointment(appointment.id)}>
                    Cancel
                    </button>
                    <button
                    className="btn btn-success"
                    onClick={() => handleFinishAppointment(appointment.id)}>
                      Finish
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

  export default AppointmentsList;
