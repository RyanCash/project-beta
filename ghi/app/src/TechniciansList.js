import React from "react";

function TechniciansList(props) {
  const handleDeleteTechnician = async (id) => {
    const techniciansUrl = `http://localhost:8080/api/technicians/${id}`;
    const response = await fetch(techniciansUrl, { method: "DELETE" });
    if (response.ok) {
      props.technicians.filter((technician) => technician.id !== id);
    }
  };

    return (
      <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {props.technicians?.map((technician) => {
            return (
              <tr key={technician.id}>
                <td>{ technician.first_name }</td>
                <td>{ technician.last_name }</td>
                <td>{ technician.employee_id}</td>
                <td className="w-25">
              </td>
              <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => handleDeleteTechnician(technician.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
    );
  }

  export default TechniciansList;
