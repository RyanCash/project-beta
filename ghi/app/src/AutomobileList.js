import React from 'react';
import { useEffect, useState } from "react";

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setAutomobiles(data.autos)
        }
    }
    useEffect(() => {
        fetchData();
        }, []);


    return (
    <>
    <h1>Automobiles</h1>
    <table className="table table-image">
        <thead>
            <tr>
                <th>VIN</th>
                <th>Model</th>
                <th>Color</th>
                <th>Year</th>
                <th>Manufacturer</th>
            </tr>
        </thead>
        <tbody>
            {automobiles.map(automobile => {
                return (
                <tr key={ automobile.id} >
                    <td>{ automobile.vin }</td>
                    <td>{ automobile.model.name }</td>
                    <td>{ automobile.color }</td>
                    <td>{ automobile.year }</td>
                    <td>{ automobile.model.manufacturer.name }</td>
                </tr>
                );
            })}
        </tbody>
    </table>
    </>
    )
};
export default AutomobileList