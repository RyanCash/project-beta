from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True, null=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True, null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} ({self.employee_id})"


class Appointment(models.Model):
    date_time = models.DateTimeField(null=True)
    reason = models.TextField()
    status = models.CharField(max_length=100, default="Created")
    vin = models.CharField(max_length=17, unique=True, null=True)
    customer = models.CharField(max_length=100, null=True) 
    technician = models.ForeignKey(
        Technician,
        on_delete=models.CASCADE,
        related_name='appointments',
        null=True
    )
