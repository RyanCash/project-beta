from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
import json
from common.json import ModelEncoder



class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]



class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "id",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
    def get_extra_data(self, o):
        count = AutomobileVO.objects.filter(vin=o.vin).count()
        return {
            "VIP": count > 0,
        }



@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
            status=200
        )
    else:
        content = json.loads(request.body)
        technician_id = content.get("employee_id")
        if Technician.objects.filter(employee_id=technician_id).exists():
            response = JsonResponse(
                {"message": "Technician ID already exists"},
                status=400,
            )
            return response
        else:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
                status=201
            )

@require_http_methods(["GET", "DELETE"])
def api_show_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
            status=200
        )
    else:
        results = Technician.objects.filter(id=id).delete()
        count = results[0]
        if count > 0:
            return JsonResponse({"deleted": count > 0}, status=204)
        else:
            return JsonResponse({"message": "Technician not found"}, status=404)

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False,
            status=200
        )
    else:
        content = json.loads(request.body)
        vin = content.get("vin")
        if Appointment.objects.filter(vin=vin).exists():
            response = JsonResponse(
                {"error": "Bad request"},
                status=400,
            )
            return response
        else:
            technician = None
            technician_id = content["technician"]
            technician = Technician.objects.get(id=int(technician_id))
            content.pop("technician")
            appointment = Appointment.objects.create(technician=technician, **content)
            appointment.status = "created"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
                status=201
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_detail_appointments(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
    except Appointment.DoesNotExist:
        return JsonResponse({"error": "Appointment does not exist"}, status=404)

    if request.method == "GET":
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"deleted": True})
        else:
            return JsonResponse({"deleted": False}, status=404)
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            Appointment.objects.filter(id=id).update(**content)
            appt = Appointment.objects.get(id=id)
            return JsonResponse(
                appt,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=400)
    else:
        return JsonResponse({"error": "Bad request"}, status=400)

@require_http_methods(["PUT"])
def api_cancel_appointments(request, id):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=id)
        appointment.status = "canceled"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        return JsonResponse({"error": "Bad request"}, status=400)


@require_http_methods(["PUT"])
def api_finish_appointments(request, id):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=id)
        appointment.status = "finished"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    else:
        return JsonResponse({"error": "Bad request"}, status=400)